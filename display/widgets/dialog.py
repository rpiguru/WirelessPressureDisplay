from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty, ObjectProperty
from kivy.uix.modalview import ModalView
from kivymd.dialog import MDDialog


Builder.load_file('widgets/kv/dialog.kv')


class PasswordDialog(MDDialog):
    pwd = StringProperty('')
    is_error = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.register_event_type('on_success')
        super(PasswordDialog, self).__init__(**kwargs)

    def on_open(self):
        self.clear_error()
        self.ids.txt_pwd.text = ''
        self.ids.txt_pwd.focus = True

    def on_success(self, *args):
        pass

    def on_ok(self):
        if self.ids.txt_pwd.text == self.pwd:
            self.dismiss()
            self.dispatch('on_success')
        else:
            self.is_error = True

    def clear_error(self):
        self.is_error = False

    def on_touch_down(self, touch):
        if self.is_error:
            self.ids.txt_pwd.text = ''
            self.is_error = False
        super(PasswordDialog, self).on_touch_down(touch)


class LoadingDialog(ModalView):
    pass


class YesNoDialog(MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(YesNoDialog, self).__init__(**kwargs)

    def on_yes(self):
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass


class InputDialog(MDDialog):

    text = StringProperty('')
    hint_text = StringProperty('')
    input_filter = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(InputDialog, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self.enable_focus(), .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self):
        self.ids.input.focus = True


class TextInputDialog(MDDialog):

    text = StringProperty('')
    max_length = NumericProperty(50)

    def __init__(self, **kwargs):
        super(TextInputDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')
        Clock.schedule_once(lambda dt: self.enable_focus(), .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self):
        self.ids.input.focus = True
