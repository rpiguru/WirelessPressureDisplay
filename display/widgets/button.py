from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image
from kivymd.button import MDRaisedButton
from kivymd.ripplebehavior import RectangularRippleBehavior
from widgets.long_touch import LongTouchBehavior


class WPDMDButton(LongTouchBehavior, MDRaisedButton):
    pass


class WPDImageButton(RectangularRippleBehavior, ButtonBehavior, Image):
    pass


class WPDLongTouchImageButton(LongTouchBehavior, WPDImageButton):
    pass
