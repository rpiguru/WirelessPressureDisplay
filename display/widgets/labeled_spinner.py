# -*- coding: iso8859-15 -*-
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ListProperty, StringProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout


Builder.load_file('widgets/kv/labeled_spinner.kv')


class LabeledSpinner(BoxLayout):

    title = StringProperty()
    values = ListProperty()
    label_width = NumericProperty(120)

    def __init__(self, **kwargs):
        super(LabeledSpinner, self).__init__(**kwargs)
        self.register_event_type('on_changed')
        Clock.schedule_once(lambda dt: self._bind_child())

    def _bind_child(self):
        self.ids.spinner.bind(on_changed=self._on_spinner_changed)

    def _on_spinner_changed(self, *args):
        self.dispatch('on_changed')

    def get_value(self):
        return self.ids.spinner.value

    def set_value(self, val):
        self.ids.spinner.set_value(val)

    def on_changed(self):
        pass
