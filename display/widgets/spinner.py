# -*- coding: iso8859-15 -*-
from kivy.lang import Builder
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.spinner import Spinner
from kivy.clock import Clock
from kivymd.button import MDRaisedButton


Builder.load_file('widgets/kv/spinner.kv')


class WPDSpinnerOption(MDRaisedButton):
    capitalized = False
    font_style = 'Subhead'


class WPDSpinner(MDRaisedButton, Spinner):
    option_cls = ObjectProperty(WPDSpinnerOption)
    capitalized = False
    font_style = 'Subhead'
    value = StringProperty()

    def __init__(self, **kwargs):
        super(WPDSpinner, self).__init__(**kwargs)
        self.register_event_type('on_changed')

    def on_text(self, instance, value):
        super(WPDSpinner, self).on_text(instance, value)
        self.value = self.text

    def _update_dropdown(self, *largs):
        super(WPDSpinner, self)._update_dropdown(*largs)
        Clock.schedule_once(lambda dt: self._update_width())

    def _update_width(self):
        for wid in self._dropdown.walk(restrict=True):
            if isinstance(wid, self.option_cls):
                wid.width = self.width

    def _on_dropdown_select(self, *args):
        super(WPDSpinner, self)._on_dropdown_select(*args)
        self.dispatch('on_changed')

    def on_values(self, *args):
        if self.values:
            self.text = self.values[0]

    def get_value(self):
        return self.value

    def set_value(self, val):
        self.text = val

    def on_changed(self):
        pass
