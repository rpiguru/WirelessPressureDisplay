from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty
from kivy.uix.relativelayout import RelativeLayout


Builder.load_file('widgets/kv/round_gauge.kv')


class WPDRoundGauge(RelativeLayout):

    value = NumericProperty()
    unit = StringProperty()
    font_size = NumericProperty(20)
    min = NumericProperty()
    max = NumericProperty(100)
    img_empty_path = StringProperty('widgets/gauge_empty.png')
    img_full_path = StringProperty('widgets/gauge_full.png')

    _angle_start = NumericProperty(-180)

    def __init__(self, **kwargs):
        super(WPDRoundGauge, self).__init__(**kwargs)

    def on_value(self, *args):
        self._angle_start = -135 + int(270 * (self.value - self.min) / float(self.max - self.min))

    def set_value(self, val):
        self.value = val


if __name__ == '__main__':
    from kivy.uix.slider import Slider
    from kivy.app import App
    from kivy.uix.boxlayout import BoxLayout


    class GaugeApp(App):
        gauge = None

        def build(self):
            box = BoxLayout(orientation='horizontal', padding=5)
            self.gauge = WPDRoundGauge(value=50)

            stepper = Slider(min=1, max=100)
            stepper.bind(
                value=lambda instance, value: setattr(self.gauge, 'value', value)
            )

            box.add_widget(self.gauge)
            box.add_widget(stepper)
            return box


    GaugeApp().run()
