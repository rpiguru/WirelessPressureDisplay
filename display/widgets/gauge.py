# -*- coding: iso8859-15 -*-

from kivy.clock import Clock
from kivy.properties import NumericProperty, ListProperty
from kivy.properties import StringProperty
from kivy.properties import BoundedNumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivy.uix.scatter import Scatter
from kivy.uix.image import Image
from kivy.uix.label import Label


class WPDGauge(Widget):
    unit = NumericProperty(1.8)
    value = BoundedNumericProperty(0, min=0, max=2000, errorvalue=0)

    gauge_img_path = StringProperty('assets/images/gauge/cadran.png')
    needle_img_path = StringProperty("assets/images/gauge/needle.png")
    text_size = NumericProperty(20)
    text_unit = StringProperty('%')
    text_color = ListProperty([0, 0, 0, 1])

    def __init__(self, **kwargs):
        super(WPDGauge, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self._add_widgets())

    def _add_widgets(self):
        self._gauge = Scatter(
            size=self.size,
            do_rotation=False,
            do_scale=False,
            do_translation=False
        )

        _img_gauge = Image(
            source=self.gauge_img_path,
            allow_stretch=True,
            size_hint=(None, None),
            size=self.size
        )

        self._needle = Scatter(
            size=self.size,
            do_rotation=False,
            do_scale=False,
            do_translation=False
        )

        _img_needle = Image(
            source=self.needle_img_path,
            allow_stretch=True,
            size=self.size
        )

        self._glab = Label(font_size=self.text_size, markup=True, color=self.text_color)

        self._gauge.add_widget(_img_gauge)
        self._needle.add_widget(_img_needle)

        self.add_widget(self._gauge)
        self.add_widget(self._needle)
        self.add_widget(self._glab)

        self.bind(pos=self._update)
        self.bind(size=self._update)
        self.bind(value=self._turn)
        self._update()
        self._turn()

    def _update(self, *args):
        """
        Update gauge and needle positions after sizing or positioning.
        """
        self._gauge.pos = self.pos
        self._needle.pos = (self.x, self.y)
        self._needle.center = self._gauge.center
        self._glab.center_x = self._gauge.center_x
        self._glab.center_y = self._gauge.center_y - self.height / 7

    def _turn(self, *args):
        """
        Turn needle, 1 degree = 1 unit, 0 degree point start on 50 value.
        """
        self._needle.center_x = self._gauge.center_x
        self._needle.center_y = self._gauge.center_y
        self._needle.rotation = 50 * self.unit - self.unit * self.value / 2000. * 100.
        self._glab.text = "[b]{0:.0f} {unit}[/b]".format(self.value, unit=self.text_unit)


if __name__ == '__main__':
    from kivy.uix.slider import Slider
    from kivy.app import App

    class GaugeApp(App):
        increasing = NumericProperty(1)
        begin = NumericProperty(50)
        step = NumericProperty(1)

        gauge = None
        slider = None

        def build(self):
            box = BoxLayout(orientation='horizontal', padding=5)
            self.gauge = WPDGauge(value=50, gauge_size=256, text_size=25)
            self.slider = Slider(orientation='vertical')

            stepper = Slider(min=1, max=25)
            stepper.bind(
                value=lambda instance, value: setattr(self, 'step', value)
            )

            box.add_widget(self.gauge)
            box.add_widget(stepper)
            box.add_widget(self.slider)
            Clock.schedule_interval(lambda *t: self.gauge_increment(), 0.03)
            return box

        def gauge_increment(self):
            begin = self.begin
            begin += self.step * self.increasing
            if 0 < begin < 100:
                self.gauge.value = self.slider.value = begin
            else:
                self.increasing *= -1
            self.begin = begin

    GaugeApp().run()
