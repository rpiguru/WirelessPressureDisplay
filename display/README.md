# Wireless Pressure Display with Raspberry Pi

## Installation

- Download the latest Raspbian **Stretch Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/) 
and flash your MicroSD card.

- Install Waveshare 7" touchscreen driver by following this:
    
    https://www.waveshare.com/wiki/7inch_HDMI_LCD
    
  **NOTE**: Don't edit `/boot/config.txt` file! (`#4`)

- Install git
    

        sudo apt-get update
        sudo apt-get -y install git

- Clone this repo

 
        cd ~
        git clone https://gitlab.com/rpiguru/WirelessPressureDisplay 

- Install everything:

        
        cd WirelessPressureDisplay/display
        bash setup.sh

  After the installation, you will be able to see a new WiFi AP named `WPD-[sn]`.
  (`sn` is the unique serial number of RPi)

  The GUI application will be started automatically at the next booting time.

  *NOTE*: Password of this AP is `drp_products`

- Wired Connection
    
    * Connect Adafruit ADS1115 ADC breakout(https://www.adafruit.com/product/1085) to RPi.
    
        ![Connection Guide](assets/images/ADS1115-schematics.png)
    
    * Connect Front sensor to **A0**, and Rear sensor to **A1**.
