# -*- coding: iso8859-15 -*-

SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 600

INIT_SCREEN = 'home_screen'


MQTT_HOST = 'localhost'
MQTT_PORT = 1883


ADMIN_PASSWORD = "drp/wpd"

INTERVAL = 1               # Saving interval of the sensor data(seconds)

DEBUG = False

# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
ADC_GAIN = 2. / 3.


try:
    from local_settings import *
except ImportError:
    pass
