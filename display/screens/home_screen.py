from kivy.lang import Builder
from screens.base import BaseScreen
from settings import ADMIN_PASSWORD, DEBUG
from utils.common import is_rpi
from widgets.dialog import PasswordDialog


Builder.load_file('screens/home_screen.kv')


class HomeScreen(BaseScreen):

    show_back_button = False

    def on_logo_long_pressed(self):
        if is_rpi() or not DEBUG:
            dlg = PasswordDialog(pwd=ADMIN_PASSWORD)
            dlg.bind(on_success=self.on_pwd_correct)
            dlg.open()
        else:
            self.on_pwd_correct()

    def on_pwd_correct(self, *args):
        self.switch_screen('calibration_screen', 'up')

    def update_screen(self):
        super(HomeScreen, self).update_screen()
        f_val = self.app.state.get('front', 0)
        r_val = self.app.state.get('rear', 0)
        if f_val * r_val > 0:
            self.ids.total.text = '{} [color=555555]PSI[/color]'.format(f_val + r_val)
            self.ids.ratio_front.text = \
                '{} [color=555555]%[/color]'.format(round(f_val / float(f_val + r_val) * 100., 1))
            self.ids.ratio_rear.text = \
                '{} [color=555555]%[/color]'.format(round(r_val / float(f_val + r_val) * 100., 1))

    def on_btn_settings(self):
        self.switch_screen('settings_screen', 'right')

    def on_btn_chart(self):
        self.switch_screen('chart_screen', 'down')
