# -*- coding: iso8859-15 -*-

from kivy.uix.screenmanager import ScreenManager
from screens.chart_screen import ChartScreen
from screens.error_screen import ErrorScreen
from screens.home_screen import HomeScreen
from screens.calibration_screen import CalibrationScreen
from screens.settings_screen import SettingsScreen

screens = {
    'home_screen': HomeScreen,
    'calibration_screen': CalibrationScreen,
    'error_screen': ErrorScreen,
    'chart_screen': ChartScreen,
    'settings_screen': SettingsScreen,
}


sm = ScreenManager()
