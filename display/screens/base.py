from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, BooleanProperty
from kivy.uix.screenmanager import Screen
from widgets.dialog import LoadingDialog


Builder.load_file('screens/base.kv')


class BaseScreen(Screen):
    app = ObjectProperty()
    show_back_button = BooleanProperty(True)
    loading_dlg = None

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self.app = App.get_running_app()
        self.loading_dlg = LoadingDialog()

    def on_enter(self, *args):
        super(BaseScreen, self).on_enter(*args)
        self.update_screen()

    def on_logo_long_pressed(self):
        pass

    def switch_screen(self, screen_name, direction=None):
        self.app.switch_screen(screen_name=screen_name, direction=direction)

    def on_btn_back(self):
        if self.show_back_button:
            self.switch_screen('home_screen', 'left')

    def update_screen(self):
        state = self.app.state
        for dev in state.keys():
            _key = 'gauge_{}'.format(dev)
            if _key in self.ids:
                self.ids[_key].value = state[dev]
