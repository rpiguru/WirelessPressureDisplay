from kivy.lang import Builder
from screens.base import BaseScreen
from utils.common import get_config


Builder.load_file('screens/settings_screen.kv')


class SettingsScreen(BaseScreen):

    def on_enter(self, *args):
        """
        Don't update values as we don't need here!
        :param args:
        :return:
        """
        super(BaseScreen, self).on_enter(*args)
        self.ids['chk_{}'.format(get_config()['mode'])].active = True

    def on_checkbox_pressed(self, chk_widget):
        if chk_widget.active:
            conn = 'wireless' if chk_widget.text == 'Wireless' else 'wired'
            self.app.set_connection_type(conn)
