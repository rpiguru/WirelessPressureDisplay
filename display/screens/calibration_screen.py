from functools import partial

from kivy.logger import Logger
from kivy.lang import Builder
from kivymd.snackbar import Snackbar
from screens.base import BaseScreen
from utils.constant import MQTT_ZERO
from widgets.dialog import InputDialog


Builder.load_file('screens/calibration_screen.kv')


class CalibrationScreen(BaseScreen):

    def __init__(self, **kwargs):
        super(CalibrationScreen, self).__init__(**kwargs)

    def on_btn_zero(self, dev):
        Logger.info('WPD: Setting Zero Point of {}'.format(dev.title()))
        self.app.publish_mqtt_message(topic=MQTT_ZERO.format(device=dev), payload='START')

    def on_btn_back(self):
        self.switch_screen('home_screen', 'down')

    def on_btn_calibrate(self, pos):
        title = 'Please input current pressure value of {}'.format(pos.capitalize())
        dlg = InputDialog(title=title, input_filter='float')
        dlg.bind(on_confirm=partial(self._on_calibrate_dlg_confirm, pos))
        dlg.open()

    def _on_calibrate_dlg_confirm(self, pos, dlg, value):
        if self.app.state[pos] == 0:
            Snackbar(text="  Can't Calibrate while its reading value is 0!  ", background_color=(.8, 0, .3, .5)).show()
        else:
            Logger.info('WPD: Calibrating `{}`, new val: {}'.format(pos, value))
            self.app.calibrate_sensor(pos=pos, new_val=float(value))
            Snackbar(text="Calibrated {}".format(pos.capitalize())).show()
