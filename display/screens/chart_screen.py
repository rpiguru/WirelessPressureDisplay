import datetime
from functools import partial
import xlsxwriter
import threading
from kivy.uix.boxlayout import BoxLayout
from kivy.utils import get_color_from_hex as rgb
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, ObjectProperty
from kivymd.snackbar import Snackbar
from screens.base import BaseScreen
from settings import INTERVAL
from utils.common import is_rpi
from utils.db import get_sensor_data, drop_db
from utils.usb import get_connected_usb_device, save_file_to_usb_drive
from widgets.dialog import YesNoDialog
from widgets.graph import SmoothLinePlot
from widgets.time_series_graph import TimeSeriesGraph


Builder.load_file('screens/chart_screen.kv')


graph_theme = {
    'label_options': {
        'color': rgb('444444'),                 # color of tick labels and titles
        'bold': True
    },
    'background_color': rgb('f8f8f2'),          # background color of canvas
    'tick_color': rgb('A0A0A0'),                # ticks and grid
    'border_color': rgb('808080')               # border drawn around each graph
}


class ChartScreen(BaseScreen):

    cur_wheel = StringProperty('LF')
    cur_dev_type = StringProperty('weight_wheel')
    _clk_chart = ObjectProperty(allownone=True)

    def on_enter(self, *args):
        super(ChartScreen, self).on_enter(*args)
        if self.app.is_recording:
            self._clk_chart = Clock.schedule_interval(lambda dt: self._update_chart(), INTERVAL)
        self.ids.btn_play.icon = 'pause' if self.app.is_recording else 'play'

    def _update_chart(self):
        self.ids.box.clear_widgets()
        layout = BoxLayout(orientation='vertical')

        data = get_sensor_data()
        if len(data) > 30:  # Show only last 100 elements
            data = data[-30:]
        x_values = list(range(len(data)))
        date_values = [datetime.datetime.fromtimestamp(d['ts']) for d in data]
        front_values = [d['front'] for d in data]
        rear_values = [d['rear'] for d in data]

        if len(x_values) > 1:
            graph = TimeSeriesGraph(
                date_values,
                date_label_format='%H:%M:%S',
                xlabel='',
                ylabel='',
                x_ticks_major='time',
                y_ticks_major=200,
                y_grid_label=True,
                x_grid_label=True,
                padding=10,
                xlog=False,
                ylog=False,
                x_grid=True,
                y_grid=True,
                xmin=0,
                xmax=max(x_values) if max(x_values) != min(x_values) else min(x_values) + 1,
                ymin=0,
                ymax=2000,
                _with_stencilbuffer=False,
                font_size='12sp',
                **graph_theme)

            plot_front = SmoothLinePlot(color=rgb('FF0000'))
            plot_front.points = zip(x_values, front_values)
            graph.add_plot(plot_front)

            plot_rear = SmoothLinePlot(color=rgb('0000FF'))
            plot_rear.points = zip(x_values, rear_values)
            graph.add_plot(plot_rear)

            layout.add_widget(graph)
            self.ids.box.add_widget(layout)
            self.ids.label_box.opacity = 1
        else:
            self.ids.label_box.opacity = 0

    def on_btn_download(self):
        dev_path = get_connected_usb_device()
        if dev_path is None and is_rpi():
            Snackbar(text="   Cannot find any USB Drive!   ", background_color=(.8, 0, .3, .5)).show()
            return
        self.loading_dlg.open()
        threading.Thread(target=self._save_data_to_usb, args=(dev_path, )).start()

    def _save_data_to_usb(self, dev_path):
        # Create a xlsx file and export data.
        file_name = 'WPD_Export_{}.xlsx'.format(datetime.datetime.now().isoformat().replace(':', '-').split('.')[0])
        file_path = '/tmp/' + file_name
        workbook = xlsxwriter.Workbook(file_path)
        data = get_sensor_data()
        worksheet = workbook.add_worksheet('WPD')
        worksheet.write(0, 0, 'DateTime')
        worksheet.write(0, 1, 'Front')
        worksheet.write(0, 2, 'Rear')
        for i, d in enumerate(data):
            worksheet.write(i + 1, 0, datetime.datetime.fromtimestamp(d['ts']).isoformat())
            worksheet.write(i + 1, 1, d['front'])
            worksheet.write(i + 1, 2, d['rear'])
        workbook.close()

        if is_rpi():
            result = save_file_to_usb_drive(dev_path, file_path)
        else:
            result = True
        Clock.schedule_once(partial(self._show_result, result, file_name))

    def _show_result(self, result, file_name, *args):
        self.loading_dlg.dismiss()
        if result:
            Clock.schedule_once(lambda dt: Snackbar(text="Successfully exported - {}".format(file_name), ).show())
        else:
            Clock.schedule_once(lambda dt: Snackbar(text="Failed to export", background_color=(.8, 0, .3, .5)).show())

    def on_btn_clear(self):
        dlg = YesNoDialog(message='Are you sure to clear all data?', title='Wireless Pressure Display')
        dlg.bind(on_confirm=self.on_confirm_clear)
        dlg.open()

    def on_confirm_clear(self, *args):
        self.ids.box.clear_widgets()
        threading.Thread(target=drop_db).start()
        Snackbar(text="Cleared all data").show()
        args[0].dismiss()

    def on_btn_play(self, btn):
        if btn.icon == 'pause':
            btn.icon = 'play'
            self.app.stop_recording()
            if self._clk_chart:
                self._clk_chart.cancel()
                self._clk_chart = None
            Snackbar(text='Stopped Recording').show()
        else:
            btn.icon = 'pause'
            self.app.start_recording()
            self._clk_chart = Clock.schedule_interval(lambda dt: self._update_chart(), INTERVAL)
            Snackbar(text='Started Recording').show()

    def on_btn_back(self):
        self.switch_screen('home_screen', 'up')
