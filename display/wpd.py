import gc
import datetime
import random
import subprocess
import traceback

# Import this before Kivy
import conf.config_before
from kivy.app import App
# Import after Kivy
import conf.config_kivy
import widgets.factory_reg

from kivy.clock import mainthread, Clock
from kivy.logger import Logger
from kivy.properties import DictProperty, ObjectProperty, StringProperty, BooleanProperty
from kivy.uix.screenmanager import SlideTransition, NoTransition
from kivy.base import ExceptionHandler, ExceptionManager
from kivymd.theming import ThemeManager
from screens.screen_manager import screens, sm
from utils.ads1115 import read_adc
from utils.common import is_rpi, get_free_gpu_size, disable_screen_saver, check_running_proc, get_config, \
    update_config_file
from utils.db import save_sensor_data
from settings import *
from paho.mqtt.client import Client
from utils.constant import MQTT_PRESSURE


mqtt_client = Client()


class WPDExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        Logger.exception(exception)
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(WPDExceptionHandler())


class WirelessPressureDisplayApp(App):

    current_screen = None
    exception = None
    theme_cls = ThemeManager()
    state = DictProperty({'front': 0, 'rear': 0})
    _clk_wired = ObjectProperty(None, allownone=True)
    conn_type = StringProperty()

    _clk_store = ObjectProperty(None, allownone=True)

    is_recording = BooleanProperty(False)

    def build(self):
        mqtt_client.on_connect = on_mqtt_connected
        mqtt_client.on_message = self.on_mqtt_messaged
        mqtt_client.connect(MQTT_HOST, MQTT_PORT)
        mqtt_client.loop_start()

        self.switch_screen(INIT_SCREEN)
        self.set_connection_type(get_config()['mode'])
        return sm

    def switch_screen(self, screen_name, direction=None, duration=.3):
        if sm.has_screen(screen_name):
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction, duration=duration)
            else:
                sm.transition = NoTransition()

            sm.switch_to(screen)
            msg = ' :: GPU - {}'.format(get_free_gpu_size()) if is_rpi() else ''
            Logger.info('WPD: ===== Switched to {} screen {} ====='.format(screen_name, msg))
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def on_mqtt_messaged(self, *args):
        topic = args[2].topic
        msg = args[2].payload.decode('utf-8')
        self.parse_mqtt_message(topic, msg)

    @mainthread
    def parse_mqtt_message(self, topic, value):
        Logger.debug('MQTT: {} :: New message - `{} - {}`'.format(datetime.datetime.now(), topic, value))
        t = topic.split('/')
        dev = t[0]
        msg_type = str(t[1]).strip()
        if msg_type == 'pressure':
            # mV to pressure
            val = float(value) * get_config()['ratio'][dev]
            self.state[dev] = int(val)
        else:
            return
        self.current_screen.update_screen()

    def save_sensor_data(self):
        if DEBUG:
            data = dict(
                front=random.randint(0, 2000),
                rear=random.randint(0, 2000),
            )
        else:
            data = dict(
                front=self.state['front'],
                rear=self.state['rear']
            )
        save_sensor_data(data=data)

    @staticmethod
    def publish_mqtt_message(topic, payload):
        Logger.info('MQTT: Publishing a message, topic: `{}`, payload: `{}`'.format(topic, payload))
        mqtt_client.publish(topic=topic, payload=payload)

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception

    def set_connection_type(self, conn_type):
        if self.conn_type != conn_type:
            self.conn_type = conn_type
            Logger.info('WPD: Switched mode - {}'.format(self.conn_type.capitalize()))
            c = get_config()
            c['mode'] = self.conn_type
            update_config_file(c)

    def read_wired_data(self):

        front_val = read_adc(0)
        # mV to pressure
        self.state['front'] = int(float(front_val) * get_config()['ratio']['front'])

        rear_val = read_adc(1)
        # mV to pressure
        self.state['rear'] = int(float(rear_val) * get_config()['ratio']['rear'])
        Logger.debug('WPD: Read wired data - {}'.format(self.state))

        self.current_screen.update_screen()

    def start_recording(self):
        self.is_recording = True
        if self._clk_store is None:
            self._clk_store = Clock.schedule_interval(lambda dt: self.save_sensor_data(), INTERVAL)
            if self.conn_type == 'wired':
                self._clk_wired = Clock.schedule_interval(lambda dt: self.read_wired_data(), INTERVAL)
            else:
                if self._clk_wired:
                    self._clk_wired.cancel()
                    self._clk_wired = None

    def stop_recording(self):
        self.is_recording = False
        if self._clk_store:
            self._clk_store.cancel()
            self._clk_store = None
        if self._clk_wired:
            self._clk_wired.cancel()
            self._clk_wired = None

    @mainthread
    def calibrate_sensor(self, pos, new_val):
        new_ratio = get_config()['ratio'][pos] * new_val / float(self.state[pos])
        c = get_config()
        c['ratio'][pos] = new_ratio
        update_config_file(c)


def on_mqtt_connected(client, userdata, flags, rc):
    Logger.info('MQTT: Connected to the Broker, Host: {}, Port: {}, UserData: {}, Flags: {}, Result code: {}'.format(
        MQTT_HOST, MQTT_PORT, userdata, flags, rc))
    for dev in ['front', 'rear']:
        mqtt_client.subscribe(MQTT_PRESSURE.format(device=dev))


def start_wpd():

    if is_rpi():
        if not check_running_proc('dnsmasq'):
            Logger.warning('WSD: dnsmasq service is not running... restarting...')
            subprocess.Popen('service dnsmasq restart', shell=True)

        if not check_running_proc('mongo'):
            Logger.warning('WSD: MongoDB service is not running... repairing...')
            _p = subprocess.Popen('sudo -u mongodb mongod --repair --dbpath /var/lib/mongodb/', shell=True,
                                  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            _p.communicate()
            _p.wait()
            subprocess.Popen('service mongodb start', shell=True)

    disable_screen_saver()

    app = WirelessPressureDisplayApp()
    app.run()
