"""
    Pre-configuration file to be used to setup Kivy configurations.
    This module must be called before executing a Kivy GUI app!
"""
import os
from kivy.clock import Clock
from kivy.config import Config
from settings import SCREEN_WIDTH, SCREEN_HEIGHT

Config.read(os.path.expanduser('~/.kivy/config.ini'))

Config.set('graphics', 'width', str(SCREEN_WIDTH))
Config.set('graphics', 'height', str(SCREEN_HEIGHT))
Config.set('kivy', 'keyboard_mode', 'systemanddock')
Config.set('kivy', 'keyboard_layout', 'en_US')
Config.set('kivy', 'log_level', 'info')
Config.set('input', 'mtdev_%(name)s', 'probesysfs,provider=mtdev')
Config.set('input', 'hid_%(name)s', 'probesysfs,provider=hidinput,param=rotation=90')
Config.remove_option('input', '%(name)s')

Clock.max_iteration = 20
