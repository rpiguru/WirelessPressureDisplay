# =========== MQTT Topics ==========
MQTT_PRESSURE = '{device}/pressure'
MQTT_CALIBRATE = '{device}/calibrate'
MQTT_ZERO = '{device}/zero'
