import time

from pymongo import MongoClient
from settings import MQTT_HOST

_client = MongoClient(MQTT_HOST, 27017)
_db = _client['WPD']
_col = _db['Pressure']


def save_sensor_data(data):
    data.update({'ts': time.time().__int__()})
    _col.insert_one(data)


def get_sensor_data():
    data = []
    for _d in list(_col.find()):
        _d.pop('_id')
        data.append(_d)
    data.sort(key=lambda d: d['ts'])
    return data


def drop_db():
    _client.drop_database('WPD')
