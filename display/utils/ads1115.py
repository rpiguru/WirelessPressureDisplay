import os
import random
import time


def is_rpi():
    return 'arm' in os.uname()[4]


if is_rpi():
    import Adafruit_ADS1x15

    # Create an ADS1115 ADC (16-bit) instance.
    adc = Adafruit_ADS1x15.ADS1115()

    # Or create an ADS1015 ADC (12-bit) instance.
    # adc = Adafruit_ADS1x15.ADS1015()

    # Note you can change the I2C address from its default (0x48), and/or the I2C
    # bus by passing in these optional parameters:
    # adc = Adafruit_ADS1x15.ADS1015(address=0x49, busnum=1)
else:
    adc = None


try:
    from settings import ADC_GAIN
except ImportError:
    ADC_GAIN = 2. / 3.


def read_adc(channel=0):
    if is_rpi():
        # Read data 10 times, and get an average value after removing minimum & maximum values.
        vals = [adc.read_adc(channel, gain=ADC_GAIN) for _ in range(10)]
        vals.remove(max(vals))
        vals.remove(min(vals))
        val = sum(vals) / float(len(vals))
        milli_volt = val * 4.096 / ADC_GAIN / 65536 * 2 * 1000
        return milli_volt
    else:
        return random.randint(0, 2000)


if __name__ == '__main__':

    print('Reading ADS1x15 values, press Ctrl-C to quit...')
    # Print nice channel column headers.
    print('| {0:>6} | {1:>6} | {2:>6} | {3:>6} |'.format(*range(4)))
    print('-' * 37)
    # Main loop.
    while True:
        # Read all the ADC channel values in a list.
        values = [0] * 4
        for i in range(4):
            # Read the specified ADC channel using the previously set gain value.
            if is_rpi():
                values[i] = adc.read_adc(i, gain=ADC_GAIN)
            else:
                values[i] = random.randint(0, 32767)
            # Note you can also pass in an optional data_rate parameter that controls
            # the ADC conversion time (in samples/second). Each chip has a different
            # set of allowed data rate values, see datasheet Table 9 config register
            # DR bit values.
            # values[i] = adc.read_adc(i, gain=GAIN, data_rate=128)
            # Each value will be a 12 or 16 bit signed integer value depending on the
            # ADC (ADS1015 = 12-bit, ADS1115 = 16-bit).
        # Print the ADC values.
        print('| {0:>6} | {1:>6} | {2:>6} | {3:>6} |'.format(*values))
        # Pause for half a second.
        time.sleep(0.5)
