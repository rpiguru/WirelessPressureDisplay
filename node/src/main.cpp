#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <EEPROM.h>
#include <Adafruit_ADS1015.h>

// ============================== Change values here ==============================

const char* ssid = "WPD-c578e9fe";                                // RPi3's AP name
//const char* ssid = "WPD-4e0d53e0";                                // RPi3's AP name


#define UPLOAD_INTERVAL       500                                 // Uploading interval of device state in miliseconds


//    --------  Constant values -----------
const char* password = "drp_products";                            // RPi3's AP password
const char* mqtt_server = "172.24.1.1";                           // Server has been built on the router(RPi 3) itself
const char* topic_pressure_front = "front/pressure";
const char* topic_pressure_rear = "rear/pressure";
const char* topic_zero_front = "front/zero";
const char* topic_zero_rear = "rear/zero";
const char* topic_calibrate_front = "front/calibrate";
const char* topic_calibrate_rear = "rear/calirate";

Adafruit_ADS1115 ads1115;
uint16_t adc_val_front = 0;
uint16_t adc_val_rear = 0;
float pressure_val_front = 0;
float pressure_val_rear = 0;

#define EEPROM_ADDR_OFFSET_FRONT  0
uint16_t position_offset_front = 0;

#define EEPROM_ADDR_OFFSET_REAR   2
uint16_t position_offset_rear = 0;

WiFiClient espClient;
PubSubClient client(espClient);

String macAddr;
char* buf_mqtt = new char[10];

unsigned long _start_time = 0;
int _elapsed_time = 0;

void setup_wifi() {
  int attempt = 0;
  delay(10);
  Serial.print("Connecting to "); Serial.println(ssid);

  WiFi.begin(ssid, password);
  macAddr = WiFi.softAPmacAddress();
  Serial.print("MAC Address: "); Serial.println(macAddr);

  while (WiFi.status() != WL_CONNECTED) {
    // Try to connect for 15 sec, and restart
    if (attempt < 30) attempt ++;
    else ESP.restart();
    // Blink LED while connecting to the router.
    if (attempt % 2 == 0) digitalWrite(D0, HIGH);
    else digitalWrite(D0, LOW);
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("WiFi connected, IP address: "); Serial.println(WiFi.localIP());
  digitalWrite(D0, HIGH);
}


void on_mqtt_message(char* topic, byte* payload, unsigned int length) {
  if (strncmp(topic_calibrate_front, topic, strlen(topic)) == 0) {
    Serial.println("Calibrating the Front Sensor");Serial.println("");
    // TODO: Implement Calibration Logic Here.
  }
  if (strncmp(topic_zero_front, topic, strlen(topic)) == 0) {
    Serial.println("Setting Zero of the Front Sensor");Serial.println("");
    position_offset_front = adc_val_front;
    EEPROM.write(EEPROM_ADDR_OFFSET_FRONT, position_offset_front / 256);
    EEPROM.write(EEPROM_ADDR_OFFSET_FRONT + 1, position_offset_front % 256);
    EEPROM.commit();
  }

  if (strncmp(topic_calibrate_rear, topic, strlen(topic)) == 0) {
    Serial.println("Calibrating the Rear Sensor");Serial.println("");
    // TODO: Implement Calibration Logic Here.
  }

  if (strncmp(topic_zero_rear, topic, strlen(topic)) == 0) {
    Serial.println("Setting Zero of the Rear Sensor");Serial.println("");
    position_offset_rear = adc_val_rear;
    EEPROM.write(EEPROM_ADDR_OFFSET_REAR, position_offset_rear / 256);
    EEPROM.write(EEPROM_ADDR_OFFSET_REAR + 1, position_offset_rear % 256);
    EEPROM.commit();
  }
}

void reconnect() {
  int attempt = 0;
  // Loop until we're reconnected
  while (!client.connected()) {
    if (attempt < 3) attempt ++;
    else ESP.restart();
    // Attempt to connect
    if (client.connect(macAddr.c_str())) {
      Serial.println("Connected to the MQTT server... ");
      client.subscribe(topic_calibrate_front);
      client.subscribe(topic_calibrate_rear);
      client.subscribe(topic_zero_front);
      client.subscribe(topic_zero_rear);
    } else {
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}


void setup() {

  Serial.begin(9600);

  EEPROM.begin(512);
  position_offset_front = EEPROM.read(EEPROM_ADDR_OFFSET_FRONT) * 256 + EEPROM.read(EEPROM_ADDR_OFFSET_FRONT + 1);
  Serial.print("Recovered the offset of Front: "); Serial.println(position_offset_front);
  position_offset_rear = EEPROM.read(EEPROM_ADDR_OFFSET_REAR) * 256 + EEPROM.read(EEPROM_ADDR_OFFSET_REAR + 1);
  Serial.print("Recovered the offset of Rear: "); Serial.println(position_offset_rear);

  pinMode(D0, OUTPUT);

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(on_mqtt_message);

  ads1115.begin();
  ads1115.setGain(GAIN_TWOTHIRDS);        // 2/3x gain   +/- 6.144V  1 bit = 0.1875mV

  ESP.wdtDisable();
  ESP.wdtEnable(WDTO_8S);
}

/*
  Read multiple(10) values from the ADC and get an average value.
*/
uint16_t readADC_MultipleValues(int ch){
  uint16_t val[10];
  long accum = 0;
  for (int i = 0; i < 10; i++){
    val[i] = ads1115.readADC_SingleEnded(ch);
    accum += val[i];
  }
  uint16_t max_val = val[0];
  uint16_t min_val = val[0];
  for (int i = 1; i < 10; i++){
    if (val[i] > max_val) max_val = val[i];
    if (val[i] < min_val) min_val = val[i];
  }
  // Remove Max & Min values and get an average value.
  return (accum - max_val - min_val) / 8;
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  _start_time = millis();

  // publish the voltage of the front sensor
  adc_val_front = readADC_MultipleValues(0);
  pressure_val_front = (float)(adc_val_front - position_offset_front) * 6.144 / 65536 * 2 * 1000;
  Serial.print("Front Pressure: ");  Serial.print(pressure_val_front); Serial.println("mV");
  sprintf(buf_mqtt, "%.3f", pressure_val_front);
  client.publish(topic_pressure_front, buf_mqtt);

  // publish the voltage of the rear sensor
  adc_val_rear = readADC_MultipleValues(1);
  pressure_val_rear = (float)(adc_val_rear - position_offset_rear) * 6.144 / 65536 * 2 * 1000;
  Serial.print("Rear Pressure: ");  Serial.print(pressure_val_rear); Serial.println("mV");
  sprintf(buf_mqtt, "%.3f", pressure_val_rear);
  client.publish(topic_pressure_rear, buf_mqtt);
  
  _elapsed_time = millis() - _start_time;
  if (_elapsed_time < UPLOAD_INTERVAL)
    delay(UPLOAD_INTERVAL - _elapsed_time);
}
