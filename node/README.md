# Wireless Pressure Device Node

## Connection Guide
    
![Breadboard View](schematic/node_bb.jpg)

![Schematic View](schematic/node_schem.jpg)

**NOTE:** 

## Programming the NodeMCU

1. Installing Visual Studio Code. (Skip this if you had already installed on your PC)

- Download and install Visual Studio Code from [here](https://code.visualstudio.com/download)

- Install **PlatformIO IDE** by following [this](http://docs.platformio.org/en/latest/ide/vscode.html#installation) link.

2. Download the source code from the gitlab repo.

- Visit https://gitlab.com/rpiguru/WirelessPressureDisplay

- Download the source code in zip format and extract all files to your hard drive.

3. Install the USB driver of the NodeMCU. (Skip this if you had already installed the driver on your PC)

    In default, Windows does not support the USB driver of the NodeMCU.
    
    Follow this link - https://cityos-air.readme.io/docs/1-usb-drivers-for-nodemcu-v10#section-13-nodemcu-v10-driver

4. Programme the NodeMCU.

- Open the **VS Code**.

- `File` -> `Open Folder`, and select the `node` directory of our repository.

- After opening successfully, you should see following:
    
    ![VS Code](../doc/photos/vscode.png)

- Open `src/main.cpp` by double clicking it, and change some values in the source code.

    * `ssid`(line 9): Find the correct SSID of your RPi and write it.
    
        How to find the SSID of your RPi?
         
        After executing the installation script on RPi, on your mobile you should be able to see a WiFi AP named `WPD-[sn]`.

- Press `Alt+Ctrl+B` to build the source code.

- Press `Alt+Ctrl+U` to upload to the NodeMCU.

- Press `Alt+Ctrl+S` to open the serial monitor and see how it works.
    
    *NOTE:* NodeMCU's blue LED will blink twice in a second when it tried to connect to the RPi thru wifi.
